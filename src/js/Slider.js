

(()=>{

    const sliderContainer = document.querySelector('.slider-wrapper');   
    const slides = sliderContainer.querySelectorAll('.slide');
    let startingPoint = 1;
    
    const duplicateSlides = () => {
            const duplicate =[slides[0].cloneNode(true),slides[1].cloneNode(true)]
            duplicate.map(el=>{
                sliderContainer.appendChild(el);
            })
    }
    
    duplicateSlides();
    
    const currentSlides = sliderContainer.querySelectorAll('.slide');
    
    const initalizeSlider=()=>{
        currentSlides.forEach((el,id)=>{
    
           el.classList.remove('active');
           el.classList.remove('prev');
           el.classList.remove('next');
    
            if(id==startingPoint){
                el.classList.add('active');
            }
            if(id==startingPoint-1){
                el.classList.add('prev');
            }
            if(id==startingPoint+1){
                el.classList.add('next');
            }
        })
    }
    
     setInterval(()=>{
        startingPoint+=1;
        if(startingPoint==currentSlides.length-1){
            startingPoint=1;
        }
        initalizeSlider();
    },3000)
    
    })();