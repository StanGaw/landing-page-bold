const form = document.querySelector('form');

if (form) {
    
    const firstName = document.getElementById('first-name');
    const lastName = document.getElementById('last-name');
    const phoneNumber = document.getElementById('phone-number');
    const selectService = document.getElementById('select-service');
    const errorRow = document.querySelector('.form__error-message');

    form.addEventListener('submit', (e) => {

        let message = []
        let name = firstName.value;
        let lastname = lastName.value;
        let number = phoneNumber.value;
        let select = selectService.value;
        let validation = false;
        let letterRegex =  /\D{2,20}/;
        let numberRegex = /[+]\d{6,20}|\d{6,20}/g;
        let phoneNumberTest = numberRegex.test(number);
        let firstNameTest = letterRegex.test(name);
        let lastNameTest = letterRegex.test(lastname);

        form.classList.remove('success');

        if (name === '' || name === null || lastname === '' || lastname === null || !firstNameTest ||
            !lastNameTest) {
            let currentMessage = '* First name and Last name are required.  (Either first name and last name are limited to 20 letters each)';
            message = [...message, currentMessage];
        }

        if (number === null || !phoneNumberTest) {
            let currentMessage = '* Phone number is required and its length should match range between 6 and 20 digits'
            message = [...message, currentMessage];
        }

        if (select == 'Select Service') {
            let currentMessage = '* Choose a service option';
            message = [...message, currentMessage];
        }

        if (message.length > 0) {
            errorRow.innerText = message.join('\n');

        } else {
            errorRow.innerText = 'Request has been sent !!!';
            validation = true;
        }

        if (validation) {
            form.classList.add('success');
        }
        
        form.classList.add('validating');
        e.preventDefault();


    })
}