const header = document.querySelector('.header');
const body = document.querySelector('body');
if(header){
    const hamburger = header.querySelector('.js-hamburger');

    const closeMenu=()=>{
        hamburger.classList.remove('is-active');
        body.classList.remove('is-active');
    }


    hamburger.addEventListener('click',(e)=>{
        hamburger.classList.toggle('is-active');
        body.classList.toggle('is-active');
    })
   
    const position = (section) =>{
        const result =  window.pageYOffset + section.getBoundingClientRect().top;
        return result;
     }

    const allSections = document.querySelectorAll('section');
    const allSectionsTriggers = header.querySelectorAll('.menu__item');
    const sectionsPosition = [];

    allSections.forEach(section=>{
        sectionsPosition.push(position(section));
        
    });

// matching scrollTrigger with section's position
    allSectionsTriggers.forEach((trigger,index)=>{

        sectionsPosition.forEach((position,positionId)=>{

            if(positionId===index&& positionId < 3){
                trigger.addEventListener('click',()=>{
                    scrollTo({
                        top:position,
                        behavior:"smooth"
                    });
                    closeMenu();
                    
                })
            }else if(positionId == 3 && index === 4){
                trigger.addEventListener('click',()=>{
                    scrollTo({
                        top:position,
                        behavior:"smooth"
                    });
                    closeMenu();
                })
            }else if(positionId == 4 && index === 3){
                trigger.addEventListener('click',()=>{
                    scrollTo({
                        top:position,
                        behavior:"smooth"
                    });
                    closeMenu();
                })
            }
            

        })
        
    })

}